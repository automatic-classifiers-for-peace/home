---
layout: ../layouts/MdLayout.astro
title: "Terms & Conditions"
author: "Allan Cheboi"
date: "06 Aug 2024"
---


## Automatic Classifiers for Peace - Terms of Use

Welcome to the Automatic Classifiers for Peace community. By accessing or using our classifiers, models, experiments, and other resources (collectively referred to as "Resources"), you agree to abide by the following terms and conditions. Please read them carefully.

## 1. Acceptance of Terms
By using the Resources provided by the Automatic Classifiers for Peace community, you acknowledge that you have read, understood, and agree to be bound by these terms of use.

## 2. Open Source License
All Resources provided by the Automatic Classifiers for Peace community are made available under the MIT License. This means you are free to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Resources, provided that the original copyright notice and this permission notice are included in all copies or substantial portions of the Resources.

## 3. Contributions
Any contributions made to the Automatic Classifiers for Peace community, including but not limited to code, documentation, and data, will be treated as open source and will be subject to the same MIT License. By contributing, you grant other members of the community the right to use, modify, and distribute your contributions.

## 4. Attribution
When using or distributing Resources from the Automatic Classifiers for Peace community, you must include appropriate attribution, including the original author(s) and a link to the original source.

## 5. No Warranty
The Resources are provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and non-infringement. In no event shall the authors or copyright holders be liable for any claim, damages, or other liability, whether in an action of contract, tort, or otherwise, arising from, out of, or in connection with the Resources or the use or other dealings in the Resources.

## 6. Compliance with Laws
You agree to use the Resources in compliance with all applicable local, state, national, and international laws, rules, and regulations.

## 7. Modifications to Terms
The Automatic Classifiers for Peace community reserves the right to modify these terms of use at any time. Any changes will be effective immediately upon posting the updated terms on our website. Your continued use of the Resources constitutes your acceptance of the modified terms.

## 8. Termination
The Automatic Classifiers for Peace community reserves the right to terminate or restrict your access to the Resources at any time, with or without cause, and without notice.

## 9. Governing Law
These terms of use are governed by and construed in accordance with the laws of the jurisdiction in which the Automatic Classifiers for Peace community is based, without regard to its conflict of law principles.

## 10. Contact Information
If you have any questions or concerns regarding these terms of use, please contact us at acfp@howtobuildup.org.

By using the Resources, you acknowledge that you have read these terms of use, understand them, and agree to be bound by them.